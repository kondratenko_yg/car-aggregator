## Операционные системы
Windows, Mac, Linux

## Програмное обеспечение
Java 11.0.7, Apache Maven 3.6.1

## Сборка проекта
```bash
mvn install
```

## Запуск приложения
```bash
java -jar ./car-aggregator-0.0.1.war
```

## Запуск контейнера для базы данных
```bash
docker-compose run --rm -p 5432:5432 postgresql
```
