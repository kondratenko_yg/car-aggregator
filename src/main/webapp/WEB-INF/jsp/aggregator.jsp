<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Cars Aggregator</title>
</head>
<body>
    <c:forEach items="${data}" var="dataItem">
    <p> ${dataItem.name}</p>
         <c:forEach items="${dataItem.models}" var="dataItemIn">
               <ul>
                    <li>
                       ${dataItemIn.name}
                    </li>
              </ul>
          </c:forEach>
    </c:forEach>
    <hr />
    <p>Available hosts:</p>
    <c:forEach items="${infoAvailable}" var="infoItem">
         <p> ${infoItem.key}</p>
    </c:forEach>
</body>
</html>