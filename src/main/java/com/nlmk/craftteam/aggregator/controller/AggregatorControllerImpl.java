package com.nlmk.craftteam.aggregator.controller;

import com.nlmk.craftteam.aggregator.entity.CarBrand;
import com.nlmk.craftteam.aggregator.service.CarBrandService;
import com.nlmk.craftteam.aggregator.service.UpdateDbService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Comparator;
import java.util.List;


@RestController
@RequestMapping("/")
public class AggregatorControllerImpl implements AggregatorController {

    private final CarBrandService carBrandService;

    private final UpdateDbService updateDbService;

    public AggregatorControllerImpl(CarBrandService carBrandService, UpdateDbService updateDbService) {
        this.carBrandService = carBrandService;
        this.updateDbService = updateDbService;
    }

    @Override
    @GetMapping("/aggregator")
    public ModelAndView getAllBrandsAndModels() {
        ModelAndView model = new ModelAndView();
        model.setViewName("aggregator");
        List<CarBrand> carBrandServiceList = carBrandService.findAll();
        carBrandServiceList.sort(Comparator.comparing(CarBrand::getName));
        model.addObject("data", carBrandServiceList);
        model.addObject("infoAvailable", updateDbService.getListAvailableHosts());
        return model;
    }

    @Override
    @GetMapping("/aggregator/new")
    public ModelAndView getAllBrandsAndModelsNew() {
        updateDbService.launchUpdateDb();
        return getAllBrandsAndModels();
    }
}