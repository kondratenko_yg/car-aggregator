package com.nlmk.craftteam.aggregator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;



@EnableCaching
@EnableScheduling
@SpringBootApplication(scanBasePackages = "com.nlmk.craftteam.aggregator")
public class CarsAggregatorApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(CarsAggregatorApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(CarsAggregatorApplication.class, args);
	}

}

