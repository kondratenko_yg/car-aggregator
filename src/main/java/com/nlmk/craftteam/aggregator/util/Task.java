package com.nlmk.craftteam.aggregator.util;

import com.nlmk.craftteam.aggregator.dto.CarBrandDTO;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.concurrent.Callable;

public class Task implements Callable<List<CarBrandDTO>> {
    private RestTemplate restTemplate;
    private String url;

    public Task(RestTemplate restTemplate, String url) {
        this.restTemplate = restTemplate;
        this.url = url;
    }

    public List<CarBrandDTO> call() {
        ResponseEntity<List<CarBrandDTO>>  brands = new RestTemplate().exchange( url, HttpMethod.GET,null, new ParameterizedTypeReference<List<CarBrandDTO>>() {
        } );

        return brands.getBody();
    }
}
