package com.nlmk.craftteam.aggregator.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="car_brand",uniqueConstraints = {@UniqueConstraint(columnNames = {"name"})})
public class CarBrand implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "brand_id")
    private Long brandId;

    @NaturalId
    private String name;

    @OneToMany(cascade = CascadeType.ALL)
    List<CarModel> models;
}
