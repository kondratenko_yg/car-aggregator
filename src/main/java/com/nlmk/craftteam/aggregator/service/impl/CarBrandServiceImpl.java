package com.nlmk.craftteam.aggregator.service.impl;

import com.nlmk.craftteam.aggregator.dto.CarBrandDTO;
import com.nlmk.craftteam.aggregator.entity.CarBrand;
import com.nlmk.craftteam.aggregator.repository.CarBrandRepository;
import com.nlmk.craftteam.aggregator.service.CarBrandService;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class CarBrandServiceImpl implements CarBrandService {

    private final CarBrandRepository carBrandRepository;

    public CarBrandServiceImpl(CarBrandRepository carBrandRepository) {
        this.carBrandRepository = carBrandRepository;
    }

    @Override
    @CachePut(value = {"brands", "brandsByName"})
    public List<CarBrand> saveList(List<CarBrand> carBrandList) {
        return carBrandRepository.saveAll(carBrandList);
    }

    @Override
    public List<CarBrand> saveAllNew(List<CarBrandDTO> body) {
        var items = body.stream()
                        .map(c -> CarBrand.builder()
                                .name(c.getName())
                                .build())
                        .filter(this::isNew)
                        .collect(Collectors.toList());
        if(items.isEmpty()) return items;
        else return saveList(items);
    }

    @Override
    public boolean isNew(CarBrand brand) {
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnorePaths("id")
                .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.ignoreCase());
        return !carBrandRepository.exists(Example.of(brand, matcher));
    }

    @Override
    @Cacheable("brandsByName")
    public Optional<CarBrand> findByName(String name) {
        return carBrandRepository.findByName(name);
    }

    @Override
    @Cacheable("brands")
    public List<CarBrand> findAll() {
        return carBrandRepository.findAll();
    }


}