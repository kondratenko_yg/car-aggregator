package com.nlmk.craftteam.aggregator.service;

import com.nlmk.craftteam.aggregator.dto.CarBrandDTO;
import com.nlmk.craftteam.aggregator.entity.CarBrand;
import com.nlmk.craftteam.aggregator.entity.CarModel;
import com.nlmk.craftteam.aggregator.util.Task;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
@Transactional
public class UpdateDbService {
    @Value("#{'${car.hosts}'.split(',')}")
    private List<String> hosts;

    @Value("#{'${car.hosts.timeout}'}")
    private Integer timeout;

    private final CarModelService carModelService;

    private final CarBrandService carBrandService;

    private Map<String, List<CarBrandDTO>> listAvailableHosts;

    public Map<String, List<CarBrandDTO>> getListAvailableHosts() {
        return listAvailableHosts;
    }

    public UpdateDbService(
            CarModelService carModelService,
            CarBrandService carBrandService) {
        this.carModelService = carModelService;
        this.carBrandService = carBrandService;
    }

    @Scheduled(fixedRate = 5 * 60 * 1000)
    public void launchUpdateDb() {
        log.info("Start update. Current time is :: " + LocalDateTime.now());
        ExecutorService executorService = Executors.newFixedThreadPool(hosts.size());
        RestTemplate restTemplate = new RestTemplate();
        listAvailableHosts = new HashMap<>();

        for (String host : hosts) {
            Future<List<CarBrandDTO>> future = executorService.submit(new Task(restTemplate, host + "/brands"));
            List<CarBrandDTO> responseBody;

            try {
                responseBody = future.get(timeout, TimeUnit.MILLISECONDS);
                if (responseBody != null) {
                    listAvailableHosts.put(host, responseBody);
                }
            } catch (Exception e) {
                log.error(host + "/brands" + " is not available");
            }
        }
        executorService.shutdown();
        listAvailableHosts.forEach((key, item) -> {
            try {
                save(key, item);
            } catch (Exception e) {
                log.error("Error while parsing and saving " + key + ". " + e.getMessage());
            }
            log.info("Updated from " + key + ". Current time is :: " + LocalDateTime.now());
        });
        log.info("Finished update. Current time is :: " + LocalDateTime.now());
    }

    private void save(String host, List<CarBrandDTO> brands) throws Exception {
        carBrandService.saveAllNew(brands);
        for (CarBrandDTO carBrand : brands) {
            ResponseEntity<List<CarModel>> models = new RestTemplate().exchange(host + "/models/" + carBrand.getId(), HttpMethod.GET, null, new ParameterizedTypeReference<List<CarModel>>() {});

            Optional<CarBrand> carBrandFromDB = carBrandService.findByName(carBrand.getName());
            List<CarModel> modelsBody = models.getBody();
            if (models.getStatusCode() == HttpStatus.OK && modelsBody != null && carBrandFromDB.isPresent()) {
                modelsBody.forEach(i -> i.setBrandId(carBrandFromDB.get().getBrandId()));

                List<CarModel> newCars = carModelService.saveAllNew(modelsBody);
                if (carBrandFromDB.get().getModels() != null && carBrandFromDB.get().getModels().size() > 0) {
                    carBrandFromDB.get().getModels().addAll(newCars);
                } else {
                    carBrandFromDB.get().setModels(newCars);
                }
            }
        }

    }

}