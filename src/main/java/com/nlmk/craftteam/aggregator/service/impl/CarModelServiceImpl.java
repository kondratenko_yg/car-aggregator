package com.nlmk.craftteam.aggregator.service.impl;


import com.nlmk.craftteam.aggregator.entity.CarModel;
import com.nlmk.craftteam.aggregator.repository.CarModelRepository;
import com.nlmk.craftteam.aggregator.service.CarModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CarModelServiceImpl implements CarModelService {

    private CarModelRepository carModelRepository;

    @Autowired
    public CarModelServiceImpl(CarModelRepository carModelRepository) {
        this.carModelRepository = carModelRepository;
    }

    @Override
    public List<CarModel> saveAllNew(List<CarModel>  body) {
        return carModelRepository.saveAll(body.stream().filter(this::isNew).collect(Collectors.toList()));
    }

    @Override
    public boolean isNew(CarModel model) {
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("brand_id",ExampleMatcher.GenericPropertyMatchers.ignoreCase())
                .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.ignoreCase());
        return !carModelRepository.exists(Example.of(model, matcher));
    }


}
