package com.nlmk.craftteam.aggregator.service;

import com.nlmk.craftteam.aggregator.entity.CarModel;

import java.util.List;

public interface CarModelService {

    List<CarModel> saveAllNew(List<CarModel>  body);

    boolean isNew(CarModel model);
}
