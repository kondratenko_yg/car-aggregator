package com.nlmk.craftteam.aggregator.service;

import com.nlmk.craftteam.aggregator.dto.CarBrandDTO;
import com.nlmk.craftteam.aggregator.entity.CarBrand;

import java.util.List;
import java.util.Optional;

public interface CarBrandService {

    List<CarBrand> saveAllNew(List<CarBrandDTO> body);

    List<CarBrand> saveList(List<CarBrand> carBrandList);

    List<CarBrand> findAll();

    Optional<CarBrand> findByName(String name);

    boolean isNew(CarBrand brand);

}
